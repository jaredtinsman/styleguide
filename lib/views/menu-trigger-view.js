import $        from 'jquery';
import Backbone from 'backbone';

export const MenuTriggerView = Backbone.View.extend({
  events: {
    'click': 'onItemClicked',
  },

  model: new Backbone.Model({
    active: false,
  }),

  initialize: function() {
    this.listenTo(this.model, 'change:active', this.onActiveChange, this);
  },

  onItemClicked: function() {
    this.model.set('active', !this.model.get('active'));
  },

  onActiveChange: function() {
    $('body').toggleClass('menu-active', this.model.get('active'));
  },
});
