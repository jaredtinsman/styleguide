import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/dropdowns.css';
import template   from '../../templates/dropdowns/layout.hbs';

export const DropdownsLayout = Marionette.LayoutView.extend({
  template: template,
});
