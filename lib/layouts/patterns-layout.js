import Marionette from 'backbone.marionette';
import styles     from '../../css/pages/patterns.css';
import template   from '../../templates/patterns/layout.hbs';

export const PatternsLayout = Marionette.LayoutView.extend({
  template: template
});
